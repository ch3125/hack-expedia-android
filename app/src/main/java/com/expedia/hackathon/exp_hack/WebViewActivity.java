package com.expedia.hackathon.exp_hack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {
    String param;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        WebView webView = (WebView) findViewById(R.id.webview);


        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        Intent myIntent = getIntent(); // gets the previously created intent
        if(myIntent.hasExtra("param"))
        param = myIntent.getStringExtra("param");
        else
            param="Paris";
        String fl="https://www.expedia.com/things-to-do/search?location="+param;
        Log.e("yo",fl);
        webView.loadUrl(fl);

    }
}
