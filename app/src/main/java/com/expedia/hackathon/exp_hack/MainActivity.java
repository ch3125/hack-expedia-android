package com.expedia.hackathon.exp_hack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.expedia.hackathon.exp_hack.Adapters.TrendingAdapter;
import com.expedia.hackathon.exp_hack.Rest.ApiClient;
import com.expedia.hackathon.exp_hack.Rest.ApiInterface;
import com.expedia.hackathon.exp_hack.Util.CustomRVItemTouchListener;
import com.expedia.hackathon.exp_hack.model.Trending;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    List<Trending> data;
    RecyclerView recyclerView;
    TrendingAdapter
            adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }

    @Override
    protected void onStart() {
        super.onStart();
        fill_with_data();
    }

    public List<Trending> fill_with_data() {

        data = new ArrayList<>();
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<List<Trending>> call = apiService.getTopRatedMovies();
        call.enqueue(new Callback<List<Trending>>() {
            @Override
            public void onResponse(Call<List<Trending>>call, Response<List<Trending>> response) {
               data = response.body();
                Log.e("tag", "Number of movies received: " + data.size());
                recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
                adapter = new TrendingAdapter(data, getApplication());
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
                itemAnimator.setAddDuration(1000);
                // itemAnimator.setRemoveDuration(1000);
                recyclerView.setItemAnimator(itemAnimator);
                recyclerView.addOnItemTouchListener(new CustomRVItemTouchListener(getApplicationContext(), recyclerView, new RecyclerViewItemClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Intent myIntent = new Intent(getApplicationContext(), WebViewActivity.class);
                        myIntent.putExtra("param",data.get(position).location);
                        //   myIntent.putExtra("secondKeyName","SecondKeyValue");
                        startActivity(myIntent);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

            }

            @Override
            public void onFailure(Call<List<Trending>>call, Throwable t) {
                // Log error here since request failed
                Log.e("tag", t.toString());
            }
        });
        return data;
    }
}