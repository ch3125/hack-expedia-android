package com.expedia.hackathon.exp_hack.Rest;

import com.expedia.hackathon.exp_hack.model.Trending;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("/getTrends")
    Call<List<Trending>> getTopRatedMovies();


}
